﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Timers;
using System.Collections.Generic;
using System.Linq;

using ScriptUtils.Pattern.Impl;
using ScriptUtils.Util;

using Commons.Model;

namespace Commons.Service {
    public class ConnectionService {
        private IPEndPoint address;
        private IPEndPoint destinyAddress;
        private EndPoint remoteEndPoint;

        private Dictionary<string, TimePackage<object>> ToSend;
        private Dictionary<string, TimePackage<object>> ToIgnore;

        private Socket socket;

        private byte [ ] buffer;

        private bool isRunning;
        public bool IsRunning {
            get {
                return this.isRunning;
            }
        }

        public delegate void ForwardPackage( Package<object> package, IPEndPoint source );
        private event ForwardPackage onReceivePackage;

        public ConnectionService( ) {
            this.isRunning = false;
        }

        public static string LocalIp( ) {
            using ( Socket socket = new Socket( AddressFamily.InterNetwork, SocketType.Dgram, 0 ) ) {
                socket.Connect( "8.8.8.8", 65530 );
                IPEndPoint endPoint = socket.LocalEndPoint as IPEndPoint;
                return endPoint.Address.ToString( );
            }
        }

        public void Quit( ) {
            this.isRunning = false;
            this.socket.Shutdown( SocketShutdown.Both );
            this.socket.Close( );
        }

        public void Init( string ip, int port ) {
            this.address = new IPEndPoint( IPAddress.Parse( ip ), port );
            this.remoteEndPoint = new IPEndPoint( IPAddress.Any, 0 );
            this.ToSend = new Dictionary<string, TimePackage<object>>( );
            this.ToIgnore = new Dictionary<string, TimePackage<object>>( );
        }

        public void Init( string ip, int port, string destinyIp, int destinyPort) {
            this.Init( ip, port );
            this.destinyAddress = new IPEndPoint( IPAddress.Parse( destinyIp ), destinyPort );
        }

        public void Start() {
            this.socket = new Socket( this.address.AddressFamily, SocketType.Dgram, ProtocolType.Udp );
            this.socket.Bind( this.address );
            this.buffer = new byte [ 2048 ];

            this.remoteEndPoint = new IPEndPoint( IPAddress.Any, 0 );

            Timer timer = new Timer( 250 );
            timer.AutoReset = true;
            timer.Elapsed += this.OnTimeElapsed;
            timer.Start( );

            this.socket.BeginReceiveFrom( this.buffer, 0, this.buffer.Length,
                SocketFlags.None, ref this.remoteEndPoint, new AsyncCallback( this.OnReceiveData ),
                this.socket );

            this.isRunning = true;
        }

        public void RegisterCallback( ForwardPackage forwardPackage ) {
            this.onReceivePackage += forwardPackage;
        }

        private void OnReceiveData( IAsyncResult result ) {

            EndPoint source = new IPEndPoint( IPAddress.Any, 0 );

            int bytes = this.socket.EndReceiveFrom( result, ref source );

            byte [ ] serializedPackage = Bytes.SubArray( this.buffer, 0, bytes );

            try {
                Package<object> package = ( Package<object> )Bytes.ByteArrayToObject( serializedPackage );
                if ( !this.ToIgnore.ContainsKey( package.Uuid ) ) {
                    if ( package.GuaranteeDelivery ) {
                        this.ToIgnore.Add( package.Uuid, TimePackage<object>.Builder.ATimePackage( )
                            .WithPackage( package )
                            .WithDestinyIp( ( ( IPEndPoint )source ).ToString( ) )
                            .WithElapsedTime( 0 )
                            .WithCurrentTimeout( 2000 )
                            .WithMaxTimeout( 4000 )
                            .Build( ) );
                        this.SendPackage( Package<object>.Builder.APackage( )
                            .WithCommand( Command.ACK )
                            .WithGuaranteeDelivery( false )
                            .WithPayload( "ACK" )
                            .WithUuid( package.Uuid )
                            .Build( ), ( IPEndPoint )source );
                    }
                    if(package.Command == Command.ACK ) {
                        this.ToSend.Remove( package.Uuid );
                    }
                    this.onReceivePackage( package, ( IPEndPoint )source );
                } else {
                    if ( package.GuaranteeDelivery ) {
                        this.SendPackage( Package<object>.Builder.APackage( )
                            .WithCommand( Command.ACK )
                            .WithGuaranteeDelivery( false )
                            .WithPayload( "ACK" )
                            .WithUuid( package.Uuid )
                            .Build( ), ( IPEndPoint )source );
                    }
                }
            } catch ( Exception e ) {
                throw e;
            }

            this.socket.BeginReceiveFrom( this.buffer, 0, this.buffer.Length,
                                            SocketFlags.None, ref this.remoteEndPoint,
                                            new AsyncCallback( this.OnReceiveData ),
                                            this.socket );
        }

        private void OnTimeElapsed( object sender, ElapsedEventArgs e ) {
            this.ResendPackagesWithoutAckConfirmation( );
            this.ClearToIgnoreList( );
        }

        public void ResendPackagesWithoutAckConfirmation( ) {
            List<KeyValuePair<string, TimePackage<object>>> entrys = this.ToSend.ToList<KeyValuePair<string, TimePackage<object>>>( );
            foreach ( KeyValuePair<string, TimePackage<object>> entry in entrys ) {
                if ( entry.Value.ElapsedTime + 250 >= entry.Value.CurrentTimeout ) {
                    this.ToSend [ entry.Key ] = TimePackage<object>.Builder.ATimePackage( )
                        .Like( entry.Value )
                        .WithElapsedTime( 0 )
                        .WithCurrentTimeout( entry.Value.CurrentTimeout * 2 )
                        .Build( );
                    string [ ] ipPort = entry.Value.DestinyIp.Split( ':' );
                    this.UnsecureSendPackage( entry.Value.Package, new IPEndPoint( IPAddress.Parse( ipPort [ 0 ] ), int.Parse( ipPort [ 1 ] ) ) );
                } else {
                    this.ToSend [ entry.Key ] = TimePackage<object>.Builder.ATimePackage( )
                        .Like( entry.Value )
                        .WithElapsedTime( entry.Value.ElapsedTime + 250 )
                        .Build( );
                }
                if ( this.ToSend [ entry.Key ].CurrentTimeout > this.ToSend [ entry.Key ].MaxTimeout ) {
                    this.ToSend.Remove( entry.Key );
                }
            }
        }

        public void ClearToIgnoreList( ) {
            List<KeyValuePair<string, TimePackage<object>>> entrys = this.ToIgnore.ToList<KeyValuePair<string, TimePackage<object>>>( );
            foreach ( KeyValuePair<string, TimePackage<object>> entry in entrys ) {
                if ( entry.Value.ElapsedTime + 250 >= entry.Value.CurrentTimeout ) {
                    this.ToIgnore [ entry.Key ] = TimePackage<object>.Builder.ATimePackage( )
                        .Like( entry.Value )
                        .WithElapsedTime( 0 )
                        .WithCurrentTimeout( entry.Value.CurrentTimeout * 2 )
                        .Build( );
                } else {
                    this.ToIgnore [ entry.Key ] = TimePackage<object>.Builder.ATimePackage( )
                        .Like( entry.Value )
                        .WithElapsedTime( entry.Value.ElapsedTime + 250 )
                        .Build( );
                }
                if ( this.ToIgnore [ entry.Key ].CurrentTimeout > this.ToIgnore [ entry.Key ].MaxTimeout ) {
                    this.ToIgnore.Remove( entry.Key );
                }
            }
        }

        public void SendPackage( Package<object> package ) {
            this.SendPackage( package, this.destinyAddress );
        }

        public void SendPackage( Package<object> package, IPEndPoint destiny ) {
            if ( package.GuaranteeDelivery ) {
                this.SecureSendPackage( package, destiny );
            } else {
                this.UnsecureSendPackage( package, destiny );
            }
        }

        private void SecureSendPackage( Package<object> package, IPEndPoint destiny ) {
            TimePackage<object> timePackage = TimePackage<object>.Builder.ATimePackage( )
                .WithPackage( package )
                .WithDestinyIp( destiny.ToString( ) )
                .WithElapsedTime( 0 )
                .WithCurrentTimeout( 250 )
                .WithMaxTimeout( 2000 )
                .Build( );
            this.ToSend.Add( package.Uuid, timePackage );
            this.UnsecureSendPackage( package, destiny );
        }

        private void UnsecureSendPackage( Package<object> package, IPEndPoint destiny ) {
            byte [ ] bytesSent = Bytes.ObjectToByteArray( package );
            this.socket.BeginSendTo( bytesSent, 0, bytesSent.Length, 0, destiny,
                                            new AsyncCallback( this.OnSendData ), this.socket );
        }

        private void OnSendData( IAsyncResult result ) {
            int bytesSent = this.socket.EndSendTo( result );
        }
    }
}
