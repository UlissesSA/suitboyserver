﻿using System;
using ScriptUtils.Util;

namespace Commons.Model {
    [Serializable()]
    public class User {

        protected string name;

        protected User( ) { }

        public string Name {
            get {
                return this.name;
            }
        }

        public override string ToString( ) {
            return string.Format("User{{Name={0}}}", this.name);
        }

        public override bool Equals( object obj ) {
            if ( this == obj ) {
                return true;
            }
            if ( Objects.isNull( obj ) || !( obj is User ) ) {
                return false;
            }
            User user = ( User )obj;
            return this.name.Equals( user.name );
        }

        public override int GetHashCode( ) {
            int result = 1;
            result = 31 * result + ( this.name == null ? 0 : this.name.GetHashCode( ) );
            return result;
        }

        public class Builder {

            private string name;

            private Builder() { }

            public static Builder AUser() {
                return new Builder();
            }

            public Builder WithName( string name) {
                Precondition.checkArgument( Objects.isNotNull( name ), "Name can't be null!" );
                this.name = name;
                return this;
            }

            public Builder Like(User client) {
                this.name = client.name;
                return this;
            }

            public Builder But() {
                return Builder.AUser()
                    .WithName( this.name );
            }

            public User Build() {
                Precondition.checkState( Objects.isNotNull( this.name ), "Name can't be null!" );
                User client = new User();
                client.name = this.name;
                return client;
            }
        }
    }
}