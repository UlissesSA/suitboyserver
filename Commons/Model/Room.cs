﻿using System;
using ScriptUtils.Util;

namespace Commons.Model {

    [Serializable( )]
    public class Room {
        private string name;
        private string pass;
        private User host;
        private Optional<User> client;

        private Room( ) { }

        public string Name {
            get {
                return this.name;
            }
        }

        public string Pass {
            get {
                return this.pass;
            }
        }

        public User Host {
            get {
                return this.host;
            }
        }

        public Optional<User> Client {
            get {
                return this.client;
            }
        }

        public override string ToString( ) {
            return string.Format( "Room{{Name={0}, Pass={1}, Host={2}, Client={3}}}", this.name, this.pass, this.host.ToString( ), this.client.isPresent() ? this.client .ToString():"null");
        }

        public override bool Equals( object obj ) {
            if ( this == obj ) {
                return true;
            }
            if ( Objects.isNull( obj ) || !( obj is Room ) ) {
                return false;
            }
            Room room = ( Room )obj;
            return this.name.Equals( room.name ) &&
                this.pass.Equals( room.pass ) &&
                this.host.Equals(room.host) &&
                this.client.Equals(room.client);
        }

        public override int GetHashCode( ) {
            int result = 1;
            result = 31 * result + ( this.name == null ? 0 : this.name.GetHashCode( ) );
            result = 31 * result + ( this.pass == null ? 0 : this.pass.GetHashCode( ) );
            result = 31 * result + ( this.host == null ? 0 : this.host.GetHashCode( ) );
            result = 31 * result + ( this.client == null ? 0 : this.client.GetHashCode( ) );
            return result;
        }

        [Serializable( )]
        public class Builder {

            private string name;
            private string pass;
            private User host;
            private Optional<User> client;

            private Builder( ) {
                this.client = Optional<User>.absent( );
            }

            public static Builder ARoom( ) {
                return new Builder( );
            }

            public Builder WithName( string name ) {
                Precondition.checkArgument( Objects.isNotNull( name ), "Name can't be null!" );
                Precondition.checkArgument(name.Length > 0, "Name can't be empty!");
                this.name = name;
                return this;
            }

            public Builder WithPass( string pass ) {
                Precondition.checkArgument( Objects.isNotNull( pass ), "Name can't be null!" );
                this.pass = pass;
                return this;
            }

            public Builder WithHost( User host ) {
                Precondition.checkArgument( Objects.isNotNull( host ), "Host can't be null!" );
                this.host = host;
                return this;
            }

            public Builder WithUser( User user ) {
                this.client = Optional<User>.fromNullable(user);
                return this;
            }

            public Builder Like( Room room ) {
                this.name = room.name;
                this.host = room.host;
                this.client = room.client;
                return this;
            }

            public Builder But( ) {
                return Builder.ARoom( )
                    .WithUser( this.client.orNull() )
                    .WithHost( this.host )
                    .WithName( this.name );
            }

            public Room Build( ) {
                Precondition.checkState( Objects.isNotNull( this.host ), "Host can't be null!" );
                Precondition.checkState( Objects.isNotNull( this.name ), "Name can't be null!" );
                Precondition.checkState( this.name.Length > 0, "Name can't be empty!");

                Room room = new Room( );
                room.name = this.name;
                room.host = this.host;
                room.client = this.client;
                return room;
            }

            public override string ToString( ) {
                return string.Format( "RoomBuilder{{Name={0}, Host={1}, Client={2}}}", this.name, this.host.ToString( ), this.client.isPresent( ) ? this.client.ToString( ) : "null" );
            }

            public override bool Equals( object obj ) {
                if ( this == obj ) {
                    return true;
                }
                if ( Objects.isNull( obj ) || !( obj is Room ) ) {
                    return false;
                }
                Room room = ( Room )obj;
                return this.name.Equals( room.name ) &&
                    this.host.Equals( room.host ) &&
                    this.client.Equals( room.client );
            }

            public override int GetHashCode( ) {
                int result = 1;
                result = 31 * result + ( this.name == null ? 0 : this.name.GetHashCode( ) );
                result = 31 * result + ( this.host == null ? 0 : this.host.GetHashCode( ) );
                result = 31 * result + ( this.client == null ? 0 : this.client.GetHashCode( ) );
                return result;
            }
        }
    }
}
