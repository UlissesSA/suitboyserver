﻿using System;
using ScriptUtils.Util;

namespace Commons.Model {
    [Serializable( )]
    public class TimePackage<T> {
        private Package<T> package;
        private string destinyIp;
        private float elapsedTime;
        private float currentTimeout;
        private float maxTimeout;

        private TimePackage( ) { }

        public Package<T> Package {
            get {
                return this.package;
            }
        }

        public string DestinyIp {
            get {
                return this.destinyIp;
            }
        }

        public float ElapsedTime {
            get {
                return this.elapsedTime;
            }
        }

        public float CurrentTimeout {
            get {
                return this.currentTimeout;
            }
        }

        public float MaxTimeout {
            get {
                return this.maxTimeout;
            }
        }

        public override string ToString( ) {
            return string.Format( "TimePackage{{Package={0}, DestinyIp={1}, ElapsedTime={2}, " +
                "CurrentTimeout={3}, MaxTimeout={4}}}", this.package, this.destinyIp, this.elapsedTime,
                this.currentTimeout, this.maxTimeout );
        }

        public override bool Equals( object obj ) {
            if ( this == obj ) {
                return true;
            }
            if ( Objects.isNull(obj) || !( obj is TimePackage<T> ) ) {
                return false;
            }
            TimePackage<T> timePackage = ( TimePackage<T> )obj;
            return this.package.Equals( timePackage.package ) &&
                   this.destinyIp.Equals( timePackage.destinyIp ) &&
                   this.elapsedTime.Equals( timePackage.elapsedTime ) &&
                   this.currentTimeout.Equals( timePackage.currentTimeout ) &&
                   this.maxTimeout.Equals( timePackage.maxTimeout );
        }

        public override int GetHashCode( ) {
            int result = 1;
            result = 31 * result + ( this.package == null ? 0 : this.package.GetHashCode( ));
            result = 31 * result + ( this.destinyIp == null ? 0 : this.destinyIp.GetHashCode( ) );
            result = 31 * result + this.elapsedTime.GetHashCode();
            result = 31 * result + this.currentTimeout.GetHashCode( );
            result = 31 * result + this.maxTimeout.GetHashCode( );
            return result;
        }

        [Serializable( )]
        public class Builder {
            private Package<T> package;
            private string destinyIp;
            private float elapsedTime;
            private float currentTimeout;
            private float maxTimeout;

            private Builder( ) { }

            public static Builder ATimePackage( ) {
                return new Builder( );
            }

            public Builder WithPackage( Package<T> package ) {
                Precondition.checkArgument( Objects.isNotNull(package), "Package can't be null" );
                this.package = package;
                return this;
            }

            public Builder WithDestinyIp( string destinyIp ) {
                Precondition.checkArgument( Objects.isNotNull( destinyIp ), "DestinyIp can't be null" );
                this.destinyIp = destinyIp;
                return this;
            }

            public Builder WithElapsedTime( float elapsedTime) {
                this.elapsedTime = elapsedTime;
                return this;
            }

            public Builder WithCurrentTimeout(float currentTimeout) {
                this.currentTimeout = currentTimeout;
                return this;
            }

            public Builder WithMaxTimeout( float maxTimeout ) {
                this.maxTimeout = maxTimeout;
                return this;
            }

            public Builder Like(TimePackage<T> timePackage) {
                this.package = timePackage.package;
                this.destinyIp = timePackage.destinyIp;
                this.elapsedTime = timePackage.elapsedTime;
                this.currentTimeout = timePackage.currentTimeout;
                this.maxTimeout = timePackage.maxTimeout;
                return this;
            }

            public Builder But( ) {
                return Builder.ATimePackage( )
                    .WithPackage( this.package )
                    .WithDestinyIp( this.destinyIp )
                    .WithElapsedTime( this.elapsedTime )
                    .WithCurrentTimeout( this.currentTimeout )
                    .WithMaxTimeout( this.maxTimeout );
            }

            public TimePackage<T> Build( ) {
                Precondition.checkState( Objects.isNotNull( this.package ), "Package can't be null" );
                Precondition.checkState( Objects.isNotNull( this.destinyIp ), "DestinyIp can't be null" );
                TimePackage<T> timePackage = new TimePackage<T>( );
                timePackage.package = this.package;
                timePackage.destinyIp = this.destinyIp;
                timePackage.elapsedTime = this.elapsedTime;
                timePackage.currentTimeout = this.currentTimeout;
                timePackage.maxTimeout = this.maxTimeout;
                return timePackage;
            }

            public override string ToString( ) {
                return string.Format( "TimePackageBuilder{{Package={0}, DestinyIp={1}, ElapsedTime={2}, " +
                    "CurrentTimeout={3}, MaxTimeout={4}}}", this.package, this.destinyIp, this.elapsedTime,
                    this.currentTimeout, this.maxTimeout );
            }

            public override bool Equals( object obj ) {
                if ( this == obj ) {
                    return true;
                }
                if ( Objects.isNull( obj ) || !( obj is TimePackage<T> ) ) {
                    return false;
                }
                TimePackage<T> timePackage = ( TimePackage<T> )obj;
                return this.package.Equals( timePackage.package ) &&
                       this.destinyIp.Equals( timePackage.destinyIp ) &&
                       this.elapsedTime.Equals( timePackage.elapsedTime ) &&
                       this.currentTimeout.Equals( timePackage.currentTimeout ) &&
                       this.maxTimeout.Equals( timePackage.maxTimeout );
            }

            public override int GetHashCode( ) {
                int result = 1;
                result = 31 * result + ( this.package == null ? 0 : this.package.GetHashCode( ) );
                result = 31 * result + ( this.destinyIp == null ? 0 : this.destinyIp.GetHashCode( ) );
                result = 31 * result + this.elapsedTime.GetHashCode( );
                result = 31 * result + this.currentTimeout.GetHashCode( );
                result = 31 * result + this.maxTimeout.GetHashCode( );
                return result;
            }
        }
    }
}
