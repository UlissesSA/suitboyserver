﻿using System;
using ScriptUtils.Util;
using Commons.Model;

namespace Commons.Model {
    [Serializable()]
    public class RemoteUser : User{

        private string[] ips;

        private RemoteUser( ) { }

        public string[] Ips {
            get {
                return this.ips;
            }
        }

        public new class Builder {

            private string name;
            private string[] ips;

            private Builder() { }

            public static Builder ARemoteUser() {
                return new Builder();
            }

            public Builder WithName( string name) {
                Precondition.checkArgument( Objects.isNotNull( name ), "Name can't be null!" );
                this.name = name;
                return this;
            }

            public Builder WithIp ( string[] ips ) {
                Precondition.checkArgument( Objects.isNotNull( ips ), "Ips can't be null!" );
                Precondition.checkArgument(ips.Length > 0, "Ips size can't be 0!");
                this.ips = ips;
                return this;
            }

            public Builder Like(RemoteUser client) {
                this.name = client.name;
                this.ips = client.ips;
                return this;
            }

            public Builder But() {
                return Builder.ARemoteUser()
                    .WithIp( this.ips )
                    .WithName( this.name );
            }

            public RemoteUser Build() {
                Precondition.checkState( Objects.isNotNull( this.name ), "Name can't be null!" );
                Precondition.checkState( Objects.isNotNull( this.ips ), "Ip can't be null!" );
                Precondition.checkState(this.ips.Length > 0, "Ips size can't be 0!");
                RemoteUser remoteUser = new RemoteUser();
                remoteUser.ips = this.ips;
                remoteUser.name = this.name;
                return remoteUser;
            }
        }
    }
}