﻿using System;

namespace Commons.Model {
    [Serializable()]
    public class SlaveServer {
        private string ip;
        private int port;
        private int users;

        private SlaveServer() { }

        public string IP {
            get {
                return this.ip;
            }
        }

        public int Port {
            get {
                return this.port;
            }
        }

        public int Users {
            get {
                return this.users;
            }
        }

        public class Builder {
            private string ip;
            private int port;
            private int users;

            private Builder() { }

            public static Builder aSlaveServer() {
                return new Builder();
            }

            public Builder Like(SlaveServer slave) {
                return this
                    .WithIP(slave.ip)
                    .WithPort(slave.port)
                    .WithUsers(slave.users);
            }

            public Builder But() {
                return aSlaveServer()
                    .WithIP(this.ip)
                    .WithPort(this.port)
                    .WithUsers(this.users);
            }

            private Builder WithIP (string ip) {
                this.ip = ip;
                return this;
            }

            private Builder WithPort (int port) {
                this.port = port;
                return this;
            }

            private Builder WithUsers (int users) {
                this.users = users;
                return this;
            }

            public SlaveServer Build() {
                SlaveServer slave = new SlaveServer();
                slave.ip = this.ip;
                slave.port = this.port;
                slave.users = this.users;
                return slave;
            }
        }
    }
}
