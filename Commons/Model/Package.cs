﻿using System;
using ScriptUtils.Util;

namespace Commons.Model {
    [Serializable()]
    public class Package<T> {
        private string uuid;
        private Command command;
        private bool guaranteeDelivery;
        private T payload;

        private Package() { }

        public string Uuid {
            get {
                return this.uuid;
            }
        }

        public Command Command {
            get {
                return this.command;
            }
        }

        public bool GuaranteeDelivery {
            get {
                return this.guaranteeDelivery;
            }
        }

        public T Payload {
            get {
                return this.payload;
            }
        }

        public override string ToString( ) {
            return string.Format( "Package{{Uuid={0}, Command={1}, GuaranteeDelivery={2}, Payload={3}}}", this.uuid, this.command, this.guaranteeDelivery, this.payload );
        }

        public override bool Equals( object obj ) {
            if ( this == obj ) {
                return true;
            }
            if ( Objects.isNull( obj ) || !( obj is Package<T> ) ) {
                return false;
            }
            Package<T> package = ( Package<T> )obj;
            return this.uuid.Equals( package.uuid ) &&
                   this.command.Equals( package.command ) &&
                   this.guaranteeDelivery.Equals( package.guaranteeDelivery ) &&
                   this.payload.Equals( package.payload );
        }

        public override int GetHashCode( ) {
            int result = 1;
            result = 31 * result + ( this.uuid == null ? 0 : this.uuid.GetHashCode( ) );
            result = 31 * result + this.command.GetHashCode( );
            result = 31 * result + this.guaranteeDelivery.GetHashCode( );
            result = 31 * result + ( this.payload == null ? 0 : this.payload.GetHashCode( ) );
            return result;
        }

        [Serializable( )]
        public class Builder {
            private string uuid;
            private Command command;
            private bool guaranteeDelivery;
            private T payload;

            private Builder() { }

            public static Builder APackage() {
                return new Builder();
            }

            public Builder WithUuid(string uuid) {
                Precondition.checkArgument( !string.IsNullOrEmpty( uuid ), "Uuid can't be null or empty!" );
                this.uuid = uuid;
                return this;
            }

            public Builder WithCommand(Command command) {
                Precondition.checkArgument(Objects.isNotNull(command), "Command can't be null!" );
                this.command = command;
                return this;
            }

            public Builder WithGuaranteeDelivery(bool guaranteeDelivery) {
                this.guaranteeDelivery = guaranteeDelivery;
                return this;
            }

            public Builder WithPayload(T payload) {
                Precondition.checkArgument( Objects.isNotNull( payload ), "Payload can't be null!" );
                this.payload = payload;
                return this;
            }

            public Builder Like( Package<T> package ) {
                this.uuid = package.uuid;
                this.command = package.command;
                this.guaranteeDelivery = package.guaranteeDelivery;
                this.payload = package.payload;
                return this;
            }

            public Builder But() {
                return Builder.APackage()
                    .WithUuid(this.uuid)
                    .WithCommand( this.command )
                    .WithGuaranteeDelivery( this.guaranteeDelivery )
                    .WithPayload( this.payload );
            }
            
            public Package<T> Build() {
                Precondition.checkState( !string.IsNullOrEmpty( this.uuid ), "Uuid can't be null or empty!" );
                Precondition.checkState( Objects.isNotNull( this.command ), "Command can't be null!" );
                Precondition.checkState( Objects.isNotNull( this.payload ), "Payload can't be null!" );
                Package<T> package = new Package<T>();
                package.uuid = this.uuid;
                package.command = this.command;
                package.guaranteeDelivery = this.guaranteeDelivery;
                package.payload = this.payload;
                return package;
            }

            public override string ToString( ) {
                return string.Format( "PackageBuilder{{Uuid={0}, Command={1}, GuaranteeDelivery={2}, Payload={3}}}", this.uuid, this.command, this.guaranteeDelivery, this.payload );
            }

            public override bool Equals( object obj ) {
                if ( this == obj ) {
                    return true;
                }
                if ( Objects.isNull( obj ) || !( obj is Package<T> ) ) {
                    return false;
                }
                Package<T> package = ( Package<T> )obj;
                return this.uuid.Equals( package.uuid ) &&
                       this.command.Equals( package.command ) &&
                       this.guaranteeDelivery.Equals( package.guaranteeDelivery ) &&
                       this.payload.Equals( package.payload );
            }

            public override int GetHashCode( ) {
                int result = 1;
                result = 31 * result + ( this.uuid == null ? 0 : this.uuid.GetHashCode( ) );
                result = 31 * result + this.command.GetHashCode( );
                result = 31 * result + this.guaranteeDelivery.GetHashCode( );
                result = 31 * result + ( this.payload == null ? 0 : this.payload.GetHashCode( ) );
                return result;
            }
        }
    }
}
