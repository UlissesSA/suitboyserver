﻿using System;

namespace Commons.Model {
    [Serializable( )]
    public struct BasicInformation {
        public float [ ] position;
        public float [ ] rotation;
        public float animationTime;
        public int animationName;
        public int layer;
        public string target;
        public ulong version;
    }
}