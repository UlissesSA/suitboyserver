﻿using System;
namespace Commons.Model {
    [Serializable()]
    public enum Command {
        ACK,
        CREATE_ROOM,
        LIST_ROOMS,
        JOIN_ROOM,
        START_GAME,
        DELETE_ROOM,
        RELAY,
        ALIVE,
    }
}