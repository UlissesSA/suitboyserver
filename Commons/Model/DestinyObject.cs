﻿using System;
using ScriptUtils.Util;

namespace Commons.Model {
    [Serializable( )]
    public class DestinyObject {
        private BasicInformation content;
        private string destinyIp;

        public DestinyObject( ) { }

        public BasicInformation Content {
            get {
                return this.content;
            }
            set {
                this.content = value;
            }
        }

        public string DestinyIp {
            get {
                return this.destinyIp;
            }
            set {
                this.destinyIp = value;
            }
        }

        public override string ToString( ) {
            return string.Format( "DestinyObject{{content={0}, destinyIp={1}}}", this.content.ToString( ), this.destinyIp.ToString( ) );
        }

        public override bool Equals( object obj ) {
            if ( this == obj ) {
                return true;
            }
            if ( Objects.isNull( obj ) || !( obj is DestinyObject ) ) {
                return false;
            }
            DestinyObject destinyObject = ( DestinyObject )obj;
            return this.content.Equals( destinyObject.content ) &&
                this.destinyIp.Equals( destinyObject.destinyIp );
        }

        public override int GetHashCode( ) {
            int result = 1;
            result = 31 * result + ( this.content.GetHashCode( ) );
            result = 31 * result + ( this.destinyIp == null ? 0 : this.destinyIp.GetHashCode( ) );
            return result;
        }
    }
}
