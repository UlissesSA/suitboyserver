﻿using System;
using System.Net;
using System.Collections.Generic;

using ScriptUtils.Pattern.Impl;

using Commons.Model;
using Commons.Service;

namespace TestClient {
    public class Client {
        private ConnectionService connectionService;

        private Dictionary<Command, Func<Package<object>, bool>> callback;

        private byte [ ] buffer;

        private bool isRunning = false;
        public bool IsRunning {
            get {
                return this.isRunning;
            }
        }

        public Client( ) {
            this.connectionService = Singleton<ConnectionService>.Instance;
            this.callback = new Dictionary<Command, Func<Package<object>, bool>>( );
            this.isRunning = false;
        }

        public void StartClient( string ip, int port, string destinyIp, int destinyPort ) {
            this.connectionService.Init( ip, port, destinyIp, destinyPort );
            this.connectionService.Start( );
            this.connectionService.RegisterCallback( this.ForwardPackage );
            this.isRunning = true;
        }

        public void Stop( ) {
            this.connectionService.Quit( );
        }

        public void SendPackage( Package<object> package ) {
            this.connectionService.SendPackage( package );
        }

        public void SendPackage( Package<object> package, Func<Package<object>, bool> callback ) {
            this.AddCallback( package.Command, callback );
            this.SendPackage( package );
        }

        public void AddCallback( Command command, Func<Package<object>, bool> callback ) {
            if ( this.callback.ContainsKey( command ) ) {
                this.callback [ command ] = callback;
            } else {
                this.callback.Add( command, callback );
            }
        }

        private void ForwardPackage( Package<object> package, IPEndPoint source ) {
            if ( this.callback.ContainsKey( package.Command ) ) {
                Func<Package<object>, bool> func = this.callback [ package.Command ];
                if ( package.Command != Command.RELAY ) {
                    this.callback.Remove( package.Command );
                }
                func( package );
            }
        }
    }
}
