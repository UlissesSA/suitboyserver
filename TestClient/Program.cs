﻿using System;

using Commons.Model;
using Commons.Service;

namespace TestClient {
    public class Program {

        private const string clientAddress = "127.0.0.1";
        private const int clientPort = 3001;
        private const string serverAddress = "127.0.0.1";
        private const int serverPort = 3000;

        public static void Main(string[] args) {

            Client client = new Client();
            client.StartClient( ConnectionService.LocalIp( ), clientPort, ConnectionService.LocalIp( ), serverPort );

            User user = User.Builder.AUser( ).WithName( "Ulisses" ).Build( );

            Package<object> createRoom = Package<object>.Builder.APackage( )
                .WithCommand( Command.CREATE_ROOM )
                .WithGuaranteeDelivery( true )
                .WithPayload( Room.Builder.ARoom()
                .WithName("Salão")
                .WithHost(user)
                .Build())
                .WithUuid( Guid.NewGuid().ToString() )
                .Build( );

            client.SendPackage( createRoom, i => {
                return true;
            } );
            
            Package<object> listRooms = Package<object>.Builder.APackage( )
                .WithCommand( Command.LIST_ROOMS )
                .WithGuaranteeDelivery( true )
                .WithPayload( "1" )
                .WithUuid( Guid.NewGuid( ).ToString( ) )
                .Build( );

            client.SendPackage( listRooms );

            Package<object> joinRoom = Package<object>.Builder.APackage( )
                .WithCommand( Command.JOIN_ROOM )
                .WithGuaranteeDelivery( true )
                .WithPayload( Room.Builder.ARoom( )
                .WithName( "Salão" )
                .WithHost( user )
                .Build( ) )
                .WithUuid( Guid.NewGuid( ).ToString( ) )
                .Build( );

            client.SendPackage( joinRoom );

            DestinyObject destinyObject = new DestinyObject( );
            destinyObject.DestinyIp = "127.0.0.1:3001";
            destinyObject.Content = new BasicInformation();

            Package<object> package = Package<object>.Builder.APackage( )
                .WithCommand( Command.RELAY )
                .WithGuaranteeDelivery( false )
                .WithUuid("id")
                .WithPayload(
                    destinyObject
                )
                .Build( );

            client.SendPackage( package );

            Console.Read();
        }
    }
}
