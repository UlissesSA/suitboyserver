﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Timers;
using System.Collections.Generic;
using System.Linq;

using ScriptUtils.Pattern.Impl;
using ScriptUtils.Util;

using Commons.Model;
using Commons.Service;

using SlaveServer.Controller;

namespace SlaveServer {
    public class Server {

        private RelayController relayController;
        private ConnectionService connectionService;

        public Server() {
            this.relayController = Singleton<RelayController>.Instance;
            this.connectionService = Singleton<ConnectionService>.Instance;
            this.connectionService.RegisterCallback(this.ForwardPackage);
        }

        public void StartServer(string serverAddress, int port) {
            this.connectionService.Init(serverAddress, port);
            this.connectionService.Start();
        }

        public void SendPackage(Package<object> package) {
            this.connectionService.SendPackage(package);
        }

        private void ForwardPackage(Package<object> package, IPEndPoint source) {
            Console.WriteLine("Recived package: " + package.ToString());
            switch (package.Command) {
                case Command.ALIVE:
                    break;
                case Command.RELAY:
                    DestinyObject destinyObject = (DestinyObject)package.Payload;
                    string ip = destinyObject.DestinyIp.Split(':')[0];
                    int port = int.Parse(destinyObject.DestinyIp.Split(':')[1]);
                    this.relayController.RelayPackage(package, new IPEndPoint(IPAddress.Parse(ip), port));
                    break;
                default:
                    break;
            }
        }
    }
}
