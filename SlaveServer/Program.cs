﻿using System;
using System.IO;
using Commons.Service;

namespace SlaveServer {
    public class Program {
        private const int port = 3000;

        public static void Main(string[] args) {
            PrintBanner();
            Server server = new Server();
            Console.WriteLine(string.Format("Server started on Ip {0}:{1}...", ConnectionService.LocalIp(), port));
            server.StartServer(ConnectionService.LocalIp(), port);
            while (true) { }
        }

        private static void PrintBanner() {
            string textFile = AppDomain.CurrentDomain.BaseDirectory.ToString() + "/banner.txt";
            if (!File.Exists(textFile)) {
                Console.WriteLine(textFile);
                return;
            }
            using (StreamReader file = new StreamReader(textFile)) {
                string ln;
                while ((ln = file.ReadLine()) != null) {
                    Console.WriteLine(ln);
                }
                file.Close();
            }
        }
    }
}
