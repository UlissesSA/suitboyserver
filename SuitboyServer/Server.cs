﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Timers;
using System.Collections.Generic;
using System.Linq;

using ScriptUtils.Pattern.Impl;
using ScriptUtils.Util;

using Commons.Model;
using Commons.Service;
using SuitboyServer.Controller;
using SuitboyServer.Service;

namespace SuitboyServer {
    public class Server {

        private RelayController relayController;
        private RoomController roomController;
        private RoomService roomService;
        private ConnectionService connectionService;
        private int[] slavePortRange;
        private string serverAddress;
        private int port;
        private string externalIp;

        public Server() {
            this.relayController = Singleton<RelayController>.Instance;
            this.roomController = Singleton<RoomController>.Instance;
            this.connectionService = Singleton<ConnectionService>.Instance;
            this.roomService = Singleton<RoomService>.Instance;
            this.connectionService.RegisterCallback(this.ForwardPackage);
        }

        public void StartServer(string serverAddress, int port, int slaveMinPort, int slaveMaxPort, string externalIp) {
            this.serverAddress = serverAddress;
            this.port = port;
            this.slavePortRange = new int[2] { slaveMinPort, slaveMaxPort };
            this.externalIp = externalIp;

            this.connectionService.Init(serverAddress, port);

            Timer timer = new Timer(10000);
            timer.AutoReset = true;
            timer.Elapsed += this.OnTimeElapsed;
            timer.Start();

            this.connectionService.Start();
        }

        private void OnTimeElapsed(object sender, ElapsedEventArgs e) {

        }

        public void SendPackage( Package<object> package ) {
            this.connectionService.SendPackage( package );
        }

        private void ForwardPackage( Package<object> package, IPEndPoint source ) {
            Console.WriteLine( "Recived package: " + package.ToString());
            switch ( package.Command ) {
                case Command.CREATE_ROOM:
                    Room room = ( Room )package.Payload;
                    room = Room.Builder.ARoom( ).Like( room )
                        .WithHost( RemoteUser.Builder.ARemoteUser()
                        .WithIp( source.ToString() )
                        .WithName( room.Host.Name )
                        .Build() )
                        .Build();
                    this.roomController.CreateRoom( room, source );
                    break;
                case Command.LIST_ROOMS:
                    this.roomController.ListRooms( source );
                    break;
                case Command.JOIN_ROOM:
                    this.roomController.JoinRoom( ( ( Room )package.Payload ), source );
                    this.roomController.DeleteRoom( ( ( Room )package.Payload ).Name, source );
                    break;
                case Command.START_GAME:
                    break;
                case Command.DELETE_ROOM:
                    this.roomController.DeleteRoom( ( ( Room )package.Payload ).Name, source );
                    break;
                case Command.RELAY:
                    DestinyObject destinyObject = ( DestinyObject )package.Payload;
                    string ip = destinyObject.DestinyIp.Split( ':' ) [ 0 ];
                    int port = int.Parse( destinyObject.DestinyIp.Split( ':' ) [ 1 ] );
                    this.relayController.RelayPackage( package, new IPEndPoint(IPAddress.Parse(ip), port));
                    break;
                case Command.ALIVE:

                    break;
                default:
                    break;
            }
        }
    }
}