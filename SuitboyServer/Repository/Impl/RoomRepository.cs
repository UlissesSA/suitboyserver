﻿using System.Collections.Generic;
using System.Linq;

using ScriptUtils.Util;
using Commons.Model;

namespace SuitboyServer.Repository.Impl {
    public class RoomRepository : IRepository<Room> {

        private Dictionary<string, Room> rooms;

        public RoomRepository( ) {
            this.rooms = new Dictionary<string, Room>( );
        }

        public bool Create( Room room ) {
            return this.rooms.TryAdd( room.Name, room );
        }

        public List<Room> FindAll( ) {
            return this.rooms.Values.ToList();
        }

        public Optional<Room> FindOne( string name ) {
            if ( this.rooms.ContainsKey( name ) ) {
                return Optional<Room>.of(this.rooms [ name ]);
            } else {
                return Optional<Room>.absent();
            }
        }

        public bool Remove( string name ) {
            return this.rooms.Remove( name );
        }

        public void Update( Room room ) {
            this.rooms [ room.Name ] = room;
        }
    }
}