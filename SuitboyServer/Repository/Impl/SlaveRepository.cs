﻿using System;
using System.Collections.Generic;
using System.Linq;

using Commons.Model;
using ScriptUtils.Util;

namespace SuitboyServer.Repository.Impl {
    public class SlaveRepository : IRepository<SlaveServer> {
        private Dictionary<string, SlaveServer> slaves;

        public SlaveRepository() {
            this.slaves = new Dictionary<string, SlaveServer>();
        }

        public bool Create(SlaveServer entitie) {
            return this.slaves.TryAdd(entitie.IP, entitie);
        }

        public List<SlaveServer> FindAll() {
            return this.slaves.Values.ToList();
        }

        public Optional<SlaveServer> FindOne(string ip) {
            if (this.slaves.ContainsKey(ip)) {
                return Optional<SlaveServer>.of(this.slaves[ip]);
            } else {
                return Optional<SlaveServer>.absent();
            }
        }

        public bool Remove(string ip) {
            return this.slaves.Remove(ip);
        }

        public void Update(SlaveServer entitie) {
            this.slaves[entitie.IP] = entitie;
        }
    }
}
