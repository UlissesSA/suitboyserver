﻿using System.Collections.Generic;
using ScriptUtils.Util;
namespace SuitboyServer.Repository {
    public interface IRepository <T> where T: class{
        bool Create( T entitie );
        Optional<T> FindOne( string uuid );
        List<T> FindAll( );
        bool Remove( string uuid );
        void Update( T entitie );
    }
}
