﻿using System;
using System.IO;
using Commons.Service;

namespace SuitboyServer {
    public class Program {
        private const int port = 3000;

        public static void Main(string[] args) {

            int min = 3001, max = 4000;
            string externalIp = ConnectionService.LocalIp();

            try { 
                min = int.Parse(Environment.GetEnvironmentVariable("min_port"));
                max = int.Parse(Environment.GetEnvironmentVariable("max_port"));
                externalIp = Environment.GetEnvironmentVariable("external_ip");
                Console.WriteLine("{0}:{1}-{2}", externalIp, min, max);
            } catch {

            }

            PrintBanner();
            Server server = new Server( );
            Console.WriteLine(string.Format("Server started on Ip {0}:{1}...", ConnectionService.LocalIp( ), port ) );
            server.StartServer( ConnectionService.LocalIp(), port, min, max, externalIp);
            while (true) { }
        }

        private static void PrintBanner() {
            string textFile = AppDomain.CurrentDomain.BaseDirectory.ToString()+"/banner.txt";
            if (!File.Exists(textFile)) {
                Console.WriteLine(textFile);
                return;
            }
            using (StreamReader file = new StreamReader(textFile)) {
                string ln;
                while ((ln = file.ReadLine()) != null) {
                    Console.WriteLine(ln);
                }
                file.Close();
            }
        }
    }
}
