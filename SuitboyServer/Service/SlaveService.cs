﻿using System;
using System.Collections.Generic;
using System.Text;

using ScriptUtils.Util;
using ScriptUtils.Pattern.Impl;

using Commons.Model;
using SuitboyServer.Repository;
using SuitboyServer.Repository.Impl;

namespace SuitboyServer.Service {
    public class SlaveService {
        private IRepository<SlaveServer> repository;

        public SlaveService() {
            this.repository = Singleton<SlaveRepository>.Instance;
        }

        public bool Create(SlaveServer slave) {
            return this.repository.Create(slave);
        }

        public List<SlaveServer> FindAll() {
            return this.repository.FindAll();
        }

        public Optional<SlaveServer> FindOne(string ip) {
            return this.repository.FindOne(ip);
        }

        public bool Remove(string ip) {
            return this.repository.Remove(ip);
        }

        public void Update(SlaveServer slave) {
            this.repository.Update(slave);
        }
    }
}
