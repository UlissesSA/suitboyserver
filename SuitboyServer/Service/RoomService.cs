﻿using System.Collections.Generic;

using ScriptUtils.Pattern.Impl;
using ScriptUtils.Util;
using Commons.Model;
using SuitboyServer.Repository;
using SuitboyServer.Repository.Impl;

namespace SuitboyServer.Service {
    public class RoomService {
        private IRepository<Room> repository;

        public RoomService() {
            this.repository = Singleton<RoomRepository>.Instance;
        }

        public bool Create( Room room ) {
            return this.repository.Create( room );
        }

        public List<Room> FindAll( ) {
            return this.repository.FindAll();
        }

        public Optional<Room> FindOne( string name ) {
            return this.repository.FindOne(name);
        }

        public bool Remove( string name ) {
            return this.repository.Remove(name);
        }

        public void Update( Room room ) {
            this.repository.Update(room);
        }
    }
}
