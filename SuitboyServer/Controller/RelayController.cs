﻿using ScriptUtils.Pattern.Impl;
using Commons.Model;
using Commons.Service;
using System.Net;

namespace SuitboyServer.Controller {
    public class RelayController {

        private ConnectionService connectionService;

        public RelayController( ) {
            this.connectionService = Singleton<ConnectionService>.Instance;
        }

        public void RelayPackage( Package<object> package, IPEndPoint destiny ) {
            this.connectionService.SendPackage( package, destiny );
        }
    }
}
