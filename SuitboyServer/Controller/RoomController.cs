﻿using System;
using System.Net;
using System.Collections.Generic;

using Commons.Model;
using Commons.Service;
using SuitboyServer.Service;

using ScriptUtils.Util;
using ScriptUtils.Pattern.Impl;

namespace SuitboyServer.Controller {
    public class RoomController {

        private RoomService roomService;
        private ConnectionService connectionService;

        public RoomController() {
            this.roomService = Singleton<RoomService>.Instance;
            this.connectionService = Singleton<ConnectionService>.Instance;
        }

        public void CreateRoom( Room payload, IPEndPoint source ) {
            Precondition.checkState( this.connectionService.IsRunning );
            Package<object> success = Package<object>.Builder.APackage( )
                    .WithCommand( Command.CREATE_ROOM )
                    .WithGuaranteeDelivery( true )
                    .WithPayload( false )
                    .WithUuid( Guid.NewGuid().ToString( ) )
                    .Build( );
            try {
                if ( !this.roomService.Create( payload ) ) {
                    throw new Exception(string.Format("Room with name {0} already exists!", payload.Name));
                }
                success = Package<object>.Builder.APackage( ).Like( success )
                    .WithPayload( true )
                    .Build( );
            } catch ( Exception e ){
                Console.WriteLine( e.Message );
                Console.WriteLine( e.StackTrace );
                success = Package<object>.Builder.APackage( ).Like( success )
                    .WithPayload( false )
                    .Build();
            }
            this.connectionService.SendPackage( success, source );
        }

        public void JoinRoom( Room recived, IPEndPoint source ) {
            Precondition.checkState( this.connectionService.IsRunning );
            Optional<Room> optionalRoom = this.roomService.FindOne(recived.Name);

            if( !optionalRoom.isPresent() ) {
                return;
            }
            Room room = optionalRoom.get( );
            if(room.Pass != recived.Pass) {
                return;
            }
            string hostIp = (( RemoteUser )room.Host).Ips[0];
            Package<object> toHost = Package<object>.Builder.APackage( )
                    .WithCommand( Command.JOIN_ROOM )
                    .WithGuaranteeDelivery( true )
                    .WithPayload( source.ToString() )
                    .WithUuid( Guid.NewGuid( ).ToString( ) )
                    .Build( );
            Package<object> toClient = Package<object>.Builder.APackage( )
                    .WithCommand( Command.JOIN_ROOM )
                    .WithGuaranteeDelivery( true )
                    .WithPayload( hostIp )
                    .WithUuid( Guid.NewGuid( ).ToString( ) )
                    .Build( );

            Console.WriteLine( string.Format("toHost: {0}", toHost));
            Console.WriteLine( string.Format( "toClient: {0}", toClient ) );

            this.connectionService.SendPackage( toHost, new IPEndPoint( IPAddress.Parse( hostIp.Split( ':' ) [ 0 ] ), int.Parse( hostIp.Split( ':' ) [ 1 ] ) ) );
            this.connectionService.SendPackage( toClient, source );
        }

        public void DeleteRoom( string name, IPEndPoint source ) {
            Precondition.checkState( this.connectionService.IsRunning );
            this.roomService.Remove( name );
        }

        public void ListRooms( IPEndPoint source ) {
            Precondition.checkState( this.connectionService.IsRunning );
            Room [ ] rooms = this.roomService.FindAll( ).ToArray();
            Package<object> packageWithRooms = Package<object>.Builder.APackage( )
                    .WithCommand( Command.LIST_ROOMS )
                    .WithGuaranteeDelivery( true )
                    .WithPayload( rooms )
                    .WithUuid( Guid.NewGuid( ).ToString( ) )
                    .Build( );
            this.connectionService.SendPackage( packageWithRooms, source );
        }
    }
}
